// NUMBER 3 & 4 - Create fetch request and map 

async function fetchData(){
	let json = await fetch('https://jsonplaceholder.typicode.com/todos/').then((response) => response.json());
	let titlesOnly = json.map((object) => object.title);
	console.log(titlesOnly)
}
fetchData();

// --------------------------------------------------------------------

// NUMBER 5 and 6- retreiving a single to do list item and Printing a message
async function fetchOne(){
	let single = await fetch('https://jsonplaceholder.typicode.com/todos/1').then((response) => response.json());
	console.log(single)
	console.log (`The item "${single.title}" on the list has a status of "${single.completed}." `);
}
fetchOne();

// -------------------------------------------------------------------

//  NUMBER 7 - Created to do list item using POST METHOD

fetch('https://jsonplaceholder.typicode.com/todos/', {
	method: 'POST',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		completed: false,	
		title: 'Created To Do List Item',
		userID: 1
	})
}).then((response) => response.json()).then((json) => console.log(json));


//-----------------------------------------------------------------------

 // NUMBER 8 & 9 = Update to do list by changing data structure:
fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: 'PUT',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Updated To Do List Item',
		descriptop: 'To update the my to do list with a different data structure',
		id: 1,	
		status: 'Pending',
		dateCompleted: 'Pending',
		userID: 1	
		
	})
}).then((response) => response.json()).then((json) => console.log(json));


// -----------------------------------------------------
// NUMBER 10 - Create fetch using PATCH METHOD and updating the status.
fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: 'PUT',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		completed: false,
		id: 1,
		status: 'Complete',
		dateCompleted: '07/09/21',
		title: 'delectus aut autem',
		userID: 1
		
	})
}).then((response) => response.json()).then((json) => console.log(json));

// -------------------------------------------------------------

// NUMBER 12- DELETE METHOD
fetch('https://jsonplaceholder.typicode.com/todos/1',{
	methdod: 'DELETE'
})
